# -*- coding: utf-8 -*-
"""
COMP3931 - Individual Project

"""
import random 
import sys
import secrets
import os
from enum import Enum

class CardType(Enum):

	S = "Suspect"	
	W = "Weapon"
	L = "Location"

class KnowledgeType(Enum):
	##The different deductions that a player can make about each card for the other player, A-D are relevant for cards you do not own, E-F are relevant for the cards you own (to note what you have revealed), G-H refer to if cards are definitely known to either be in or not in the envelope
	
	A = "No knowledge"
	B = "Owned by"
	C = "Not owned by"
	D = "May own it"
	E = "Not revealed"
	F = "Revealed"
	G = "Not in envelope"
	H = "In envelope"

class Card:
	def __init__(self, name, card_type):
		self.name = name
		self.card_type = card_type

	def __str__(self):
		return self.name   
    
	def owner(self, state):
		for player in state.players:
			if self in player.hand:
				return player
		return None

class Player:
	def __init__(self, name, strategy, player_type):
		self.strategy = strategy
		self.game = None
		self.games_won = 0
		self.hand = []
		self.number = None
		self.name = name
		self.wrong_accusation = False
		self.knowledge_base = None
		self.suggestion_number = None
		self.type = player_type

	def display(self, state):
		display_output(self.name)
		for c in self.hand:
			display_output(c)
	
	def __str__(self):
		return self.name
			

class Deck:
	def __init__(self):
		#Suspects
		scarlett = Card("Miss Scarlett", CardType.S)
		plum = Card("Professor Plum", CardType.S)
		peacock = Card("Mrs Peacock", CardType.S)
		mustard = Card("Colonel Mustard", CardType.S)
		orchid = Card("Doctor Orchid", CardType.S)
		green = Card("Reverend Green", CardType.S)
		#Weapons
		rope = Card("Rope", CardType.W)
		dagger = Card("Dagger", CardType.W)
		wrench = Card("Wrench", CardType.W)
		revolver = Card("Revolver", CardType.W)
		candlestick = Card("Candlestick", CardType.W)		
		lead_pipe = Card("Lead Pipe", CardType.W)	
		#Locations
		kitchen = Card("Kitchen", CardType.L)
		dining_room = Card("Dining Room", CardType.L)
		lounge = Card("Lounge", CardType.L)
		hall = Card("Hall", CardType.L)
		study = Card("Study", CardType.L)
		library = Card("Library", CardType.L)
		billiard_room = Card("Billiard Room", CardType.L)
		conservatory = Card("Conservatory", CardType.L)
		ballroom = Card("Ballroom", CardType.L)

		self.cards = [scarlett, plum, peacock, mustard, orchid, green, rope, dagger, wrench, revolver, candlestick, lead_pipe, kitchen, dining_room, lounge, hall, study, library, billiard_room, conservatory, ballroom]

		self.suspects = [scarlett, plum, peacock, mustard, orchid, green]
		self.weapons = [rope, dagger, wrench, revolver, candlestick, lead_pipe]
		self.locations = [kitchen, dining_room, lounge, hall, study, library, billiard_room, conservatory, ballroom]

		self.sets = [self.suspects, self.weapons, self.locations]
		
	def get_card(self, card_name):
		for card in self.cards:
			if card.name == card_name:
				return card	

class GameState():
	def __init__(self):
		pass

	def startState(start_game):
		state = GameState()
		state.game = start_game
		state.deck = Deck()
		state.players = start_game.players.copy()
		state.number_of_players = len(state.players)
		state.number_of_asking_players = len(state.players)
		state.envelope = start_game.envelope
		state.human = start_game.human
		#suggestions dictionary, suggestion # as key, suggestion [suspect, weapon, location] and player as value
		state.suggestion_dict = {}
		#response list containing (suggestion, player) as key and response as value : true =yes has one of three, false =has none
		state.response_dict = {}

		state.round = 1
		state.current_player_number = 0
		state.phase = "start turn"
		return state

	#Display who owns what card
	def display_state(self):
		display_output()
		temp_list = []
		print_list = []
		display_output("{:<16} : {}".format("Card", "Name"))	
		display_output("--------------------------------")
		for c in self.deck.cards:
			display_output( "{:<16} : {}".format(c.name, c.owner(self)))
		display_output()

	def current_player(self):
		return self.players[self.current_player_number]

	def move(self, display):

		player = self.current_player()
		
		if self.phase == "start turn":
			if self.human:
				enter = input("PRESS ENTER TO MOVE TO NEXT PERSONS TURN")
			if player.wrong_accusation:
				self.phase = "end of turn"
				return
			if player.type == "AI":
				self.update_knowledge_base(player)
				ready_to_accuse = self.ready_to_accuse(player)
				if ready_to_accuse:
					self.phase = "accuse"
					return
				else:
					self.phase = "suggest"
					return
			else:
				decision = input("Do you want to suggest or accuse - enter Suggest or Accuse \n")
				if decision == "Suggest":
					self.phase = "suggest"
				elif decision == "Accuse":
					self.phase = "accuse"
				else:
					print("Please enter Accuse or Suggest")
				return
			
		elif self.phase == "accuse":
			suspect = weapon = location = None
			if player.type == "AI":
				for card in self.deck.suspects:
					if player.knowledge_base[card, 'overall'] == KnowledgeType.H:
						suspect = card
				for card in self.deck.weapons:
					if player.knowledge_base[card, 'overall'] == KnowledgeType.H:
						weapon = card
				for card in self.deck.locations:
					if player.knowledge_base[card, 'overall'] == KnowledgeType.H:
						location = card
			else:
				accusation = self.human_input("accuse")
				for a in accusation:
					if a == "":
						print("Incorrect input of accusation try again")
						return
				suspect = accusation[0]
				weapon = accusation[1]
				location = accusation[2]

			if suspect in self.envelope and weapon in self.envelope and location in self.envelope:
				self.phase = "solved"
				return
			else:
				print("WRONG")
				player.wrong_accusation = True
				self.number_of_asking_players -= 1
				self.suggestion_dict[player.suggestion_number]= (suspect, weapon, location, player)
				self.response_dict[suspect, weapon, location, "envelope"] = False
				display_output(player, " made a wrong accusation and can no longer suggest or ask questions")
				if(self.number_of_asking_players == 0):
					self.phase = "unsolved"
					return
				self.phase = "end of turn"
				return
		elif self.phase == "suggest":
			if player.strategy == "Basic":
				suggestion = self.generate_basic_suggestion(player)
			elif player.strategy == "Human":
				suggestionInput = self.human_input("suggestion")
				for s in suggestionInput:
					if s == "":
						print("Incorrect input of suggestion try again")
						return
				suggestion = suggestionInput	
			elif player.strategy == "Advanced":
				suggestion = self.generate_advanced_suggestion(player)	
			display_output(player, "is suggesting ", suggestion[0], " with the ", suggestion[1], " in the ", suggestion[2])
			self.suggestion_dict[player.suggestion_number]= (suggestion[0], suggestion[1], suggestion[2], player)
			self.disprove_suggestion(player, suggestion)
			player.suggestion_number = player.suggestion_number + self.number_of_asking_players
			if player.type == "AI":
				ready_to_accuse = self.ready_to_accuse(player)
			else:
				answered = False
				while not answered:
					accusing = input("Are you ready to make an accusation - please enter Y or N \n")
					if accusing == "Y":
						ready_to_accuse = True
						answered = True
					elif accusing == "N":
						ready_to_accuse = False
						answered = True
					else:
						print("Please enter Y or N")
				
			if ready_to_accuse:
				self.phase = "accuse"
				return
			else:
				display_output(player, "has finished their turn")
				self.phase = "end of turn"
				return

		elif self.phase == "end of turn":
			self.current_player_number = (self.current_player_number + 1) % len(self.players)
			self.phase = "start turn"
			return

	def update_knowledge_base(self, player):
		suggestions = self.get_suggestions_since_last_turn(player)

		for suggestion in suggestions:
			if suggestion[3] == "envelope":
				pass
			else:
				"""Currently we do not make deductions based on what a player suggests, focus instead on what the other players respond """
				player_asking_number = suggestion[3].number
				disproved = False
				count = 0
				while (not disproved) and count < self.number_of_players - 1: 
					player_responding_number = (player_asking_number + 1 + count) % self.number_of_players

					if player_responding_number == player.number or player_responding_number == player_asking_number:
						#do not need to record their own response
						pass
					else:
						player_responding = self.players[player_responding_number]
				
						if (suggestion[0], suggestion[1], suggestion[2], player_responding) in self.response_dict.keys():
							response = self.response_dict[suggestion[0], suggestion[1], suggestion[2], player_responding]
							self.update_knowledge(player, player_responding, (suggestion[0], suggestion[1], suggestion[2]), response)
							if response :
								disproved = True
					count = count + 1
	def human_input(self, inputType):
		suspect = weapon = location = ""
		suspectInput = input("Which suspect do you want to " + inputType + "? - please enter full name e.g. Miss Scarlett \n")
		for s in self.deck.suspects:
			if s.name == suspectInput:
				suspect = s

		weaponInput = input("Which weapon do you want to " + inputType + "? - please enter full name e.g. Rope \n")
		for w in self.deck.weapons:
			if w.name == weaponInput:
				weapon = w
		
		locationInput = input("Which location do you want to " + inputType + "? - please enter full name e.g. Library \n")
		for l in self.deck.locations:
			if l.name == locationInput:
				location = l
		return([suspect, weapon, location])

	def get_suggestions_since_last_turn(self, player):
		update_suggestions = []
		current_suggestion_number = player.suggestion_number
		if(len(self.suggestion_dict) >= (self.number_of_players - 1)):
			for i in range (current_suggestion_number - 1, current_suggestion_number - self.number_of_players , -1):
				update_suggestions.append(self.suggestion_dict.get(i))

		else:
			for suggestion in self.suggestion_dict.values():
				update_suggestions.append(suggestion)
		return update_suggestions
					

	def update_knowledge(self,player, response_player, suggestion, response):
		overall_values = []
		player_values = []
		count = 0
		for s in suggestion:
			overall_values.append(player.knowledge_base[s,'overall'])
			player_values.append(player.knowledge_base[s, response_player])
		
		#if they have one of the three cards
		if response:
			count = 0
			checking_counter = 0
			checking_overall = {}
			for overall in overall_values:
				#Check if card is known owned by another player
				if overall == KnowledgeType.G and (not player.knowledge_base[suggestion[count], response_player] == KnowledgeType.B):
					checking_overall[count] = True
					checking_counter += 1
				else:
					checking_overall[count] = False
				count += 1
			count = 0
			checking_player_counter = 0
			checking_player = {}
			for player_value in player_values:
				#Check if card is not owned by this player
				if player_value == KnowledgeType.C:
					checking_player[count] = True
					checking_player_counter +=1
				else:
					checking_player[count] = False
				count +=1
					

			#If two of the cards suggested are known to be owned by other players, then the player must own the third one
			if checking_counter == 2:
				for count, check in checking_overall.items():
					if not check:
						self.update_player_owns_card(player, response_player, suggestion[count])
				
			else:
				#If two of the cards are not owned by the player, then they must own the third one
				if checking_player_counter == 2:
					for count, check in checking_player.items():
						if not check:
							self.update_player_owns_card(player, response_player, suggestion[count])
				else:
					for count,check in checking_player.items():
						if (not check) and (not player.knowledge_base[suggestion[count], response_player] == KnowledgeType.B) :
							player.knowledge_base[suggestion[count], response_player] = KnowledgeType.D
				
		#if they do not have any of the cards
		else:
			for s in suggestion:
				self.update_player_does_not_own_card(player, response_player, s)

	def update_player_does_not_own_card(self, player, card_owner, card):
		player.knowledge_base[card, card_owner] = KnowledgeType.C
		possibly_owned = False
		for p in self.players:
			if not player.knowledge_base[card, p] == KnowledgeType.C:
				possibly_owned = True
		if not possibly_owned:
			player.knowledge_base[card, 'overall'] = KnowledgeType.H


	#When a player discovers that another player owns a card, update their knowledge base accordingly
	def update_player_owns_card(self, player, card_owner, card):
		player.knowledge_base[card, card_owner] = KnowledgeType.B
		player.knowledge_base[card, 'overall'] = KnowledgeType.G
		players_list = self.players.copy()
		players_list.remove(card_owner)
		for p in players_list:
			player.knowledge_base[card, p] = KnowledgeType.C

		if card in self.deck.suspects:
			self.check_category(player, self.deck.suspects)
		elif card in self.deck.weapons:
			self.check_category(player, self.deck.weapons)
		else:
			self.check_category(player, self.deck.locations)

	#Check if there is only one remaining card that is not known to be in anyone's hand for one of the categories
	def check_category(self, player, card_type):
		counter = 0
		card_in_envelope = None
		for card in card_type:
			if player.knowledge_base[card, 'overall'] == KnowledgeType.G:			
				counter += 1
			else:	
				card_in_envelope = card
		
		if counter == len(card_type) - 1:
			player.knowledge_base[card_in_envelope, 'overall'] = KnowledgeType.H
			

	def ready_to_accuse(self, player):
		suspect = False
		weapon = False
		location = False
		for card in self.deck.suspects:
			if player.knowledge_base[card, 'overall'] == KnowledgeType.H:
				suspect = True
		for card in self.deck.weapons:
			if player.knowledge_base[card, 'overall'] == KnowledgeType.H:
				weapon = True		
		for card in self.deck.locations:
			if player.knowledge_base[card, 'overall'] == KnowledgeType.H:
				location = True
		if suspect and weapon and location:
			return True
		else:
			return False
		
	def generate_basic_suggestion(self, player):
		#The user should pick a random suspect, weapon and location that are not known to be in people's hands
		suspect = self.choose_card(self.deck.suspects, player)
		weapon = self.choose_card(self.deck.weapons, player)
		location = self.choose_card(self.deck.locations, player)
		return [suspect, weapon, location]

	def generate_advanced_suggestion(self, player):
		#The user should gather a list of cards that are known to be not in the next player's hand and not in their hand
		number = (player.number + 1) % len(self.players)
		next_player = self.players[number]
		questionable_cards = []
		for card in self.deck.cards:
			if player.knowledge_base[card, next_player] == KnowledgeType.C and player.knowledge_base[card, player] == KnowledgeType.C and player.knowledge_base[card,'overall'] == KnowledgeType.A:
				questionable_cards.append(card)
		if len(questionable_cards) == 0:
			#If there are no cards that apply for this strategy, generate a basic suggestion
			return self.generate_basic_suggestion(player)
		else:
			suspect = weapon = location = ""
			suspects = [] 
			weapons = []
			locations = []
			for card in questionable_cards:
				if card.card_type == CardType.S:
					suspects.append(card)
				elif card.card_type == CardType.W:
					weapons.append(card)
				elif card.card_type == CardType.L:
					locations.append(card)
			count = 0
			if len(locations) > 0:
				location = locations.pop()
				count += 1	
			if len(weapons) > 0 :
				weapon = weapons.pop()
				count += 1
			if count < 2 and len(suspects) > 0:
				suspect = suspects.pop()
				count += 1
			if count == 1:
				if suspect == "":
					suspect = self.choose_card(self.deck.suspects, player)
				if weapon == "":
					weapon = self.choose_card(self.deck.weapons, player)
				if location == "":
					location = self.choose_card(self.deck.locations, player)
			elif count == 2:
				category = ""
				if suspect == "": 
					category = self.deck.suspects
				elif weapon == "":
					category = self.deck.weapons
				elif location == "":
					category = self.deck.locations

				number = (player.number + 1) % len(self.players)
				next_player = self.players[number]
				questionable_cards = []
				for card in category:
					if player.knowledge_base[card, next_player] == KnowledgeType.C and player.knowledge_base[card, player] == KnowledgeType.C and player.knowledge_base[card,'overall'] == KnowledgeType.A:
						questionable_cards.append(card)
				
				if len(questionable_cards) == 0:
					card = self.choose_card(category, player)
				else:
					card = questionable_cards.pop()
				if suspect == "": 
					suspect = card
				elif weapon == "":
					weapon = card
				elif location == "":
					location = card
			
		return [suspect, weapon, location]			

	def choose_card(self, category, player):
		possible_cards = []
		possibilities = 0
		for card in category:
			if player.knowledge_base[card, 'overall'] == KnowledgeType.A:
				possible_cards.append(card)
				possibilities +=1
		if possibilities == 0 :
			possible_cards = category.copy()
		random.shuffle(possible_cards)
		return possible_cards.pop()

	def disprove_suggestion(self, player, suggestion):
		number = player.number
		count = 0
		disproved = False
		while (not disproved) and count < len(self.players) - 1:
			number = (number + 1) % len(self.players)
			player_responding = self.players[number]
			if player_responding.type == "AI":
				card_in_player_hand = self.disprove_suggestion_card(player, suggestion, player_responding)
				if not card_in_player_hand:
					display_output(player_responding, "responds no")
					if player.type == "AI":
						for card in suggestion:
							self.update_player_does_not_own_card(player, player_responding, card)
					self.response_dict[suggestion[0], suggestion[1], suggestion[2], player_responding] = False
				else:
					if (self.human) and (player.type == "AI"):
						display_output(player_responding, "responds with a card")
					else:
						display_output(player_responding, "responds with card", card_in_player_hand)
						
					if player.type == "AI":
						self.update_player_owns_card(player, player_responding, card_in_player_hand)
					self.response_dict[suggestion[0], suggestion[1], suggestion[2], player_responding] = True
					disproved = True
			else:
				print("YOUR TURN")
				checkDisprove =True
				while checkDisprove:
					disproving = input("Can you disprove suggestion - please enter Y or N \n")
					if disproving == "Y":
						valid = False
						while not valid:
							cardShowing = input("Which card would you like to show - please enter card name e.g. Miss Scarlett \n")
							counter = 0						
							for s in suggestion:
								if s.name == cardShowing:
									if s in player_responding.hand:
										self.update_player_owns_card(player, player_responding, s)
										self.response_dict[suggestion[0], suggestion[1], suggestion[2], player_responding] = True
										valid = True
										checkDisprove = False
										disproved = True
									else:
										print("You do not hold this card in your hand")
										valid = True
								else:	
									counter +=1 
							if counter == 3:
								print("That card is not part of the suggestion")
					elif disproving == "N":
						counter = 0
						for s in suggestion:
							if s in player_responding.hand:
								print("You can show a card, please check your hand")
							else:
								counter += 1
						if counter == 3:
							for card in suggestion:
								self.update_player_does_not_own_card(player, player_responding, card)
							self.response_dict[suggestion[0], suggestion[1], suggestion[2], player_responding] = False
							checkDisprove = False
						
			count += 1
	
	def disprove_suggestion_card(self, player_asking, suggestion, player_responding):
		#If basic or advanced strategy, check if they have any of the suggested cards, then check if they have revealed any to the suggesting player before
		if player_responding.strategy == "Basic" or player_responding.strategy == "Advanced":
			number_of_cards_in_hand = 0
			cards_in_hand = []
			for card in suggestion:
				if card in player_responding.hand:
					number_of_cards_in_hand +=1
					cards_in_hand.append(card)
			if number_of_cards_in_hand == 0:
				#This player does not have any of the cards
				return False
			else:
				cards_shown = []
				for card in cards_in_hand:
					if player_responding.knowledge_base[card, player_asking] == KnowledgeType.F:
						#They have shown this card to this player before and should show again
						cards_shown.append(card)
				if len(cards_shown) > 0:
					#Show one of the cards again
					return cards_shown[0]
				else:
					#Show one of the cards (pick first one)
					revealed_card = cards_in_hand[0]
					player_responding.knowledge_base[card, player_asking] = KnowledgeType.F
					return cards_in_hand[0]
			

class Game:
	def __init__(self, players, randomDeal, human, deal):
		self.players = players
		self.human = human
		self.envelope = []
		self.state = GameState.startState(self)
		self.deck = self.state.deck
		self.complete = False
		remaining_cards = []	
		if randomDeal:
			for sets in self.deck.sets:
				s = sets.copy()
				for i in range(secrets.randbelow(10)):
					random.shuffle(s)
				selected = random.choice(s)
				self.envelope.append(selected)
				s.remove(selected)
				for card in s : 
					remaining_cards.append(card)
			for i in range(secrets.randbelow(10)):
				random.shuffle(remaining_cards)
			count = 0
			while len(remaining_cards) > 0:
				players[count].hand.append(remaining_cards.pop())
				count = (count + 1) % len(players)
			
		else:

			cardList = []
			for cardName in deal.get("envelope"):
				for card in self.deck.cards:
					if card.name == cardName:
						self.envelope.append(card)
			if len(players) == 3:
				count = 0	
				for player in players:
					for cardName in deal.get(count):
						for card in self.deck.cards:
							if card.name == cardName:
								player.hand.append(card)
					count +=1 
			else:
				counter = 0	
				count = 0
				for player in players:
					if counter < 3:
						for cardName in deal.get(count)[0:3]:
							for card in self.deck.cards:
								if card.name == cardName:
									player.hand.append(card)
					else:
						for cardName in deal.get(count)[3:6]:
							for card in self.deck.cards:
								if card.name == cardName:
									player.hand.append(card)
					counter +=1 
					count +=1
					if count == 3:	
						count = 0

		counter = 0
		for p in players:
			if p.type == "AI":
				p.knowledge_base = self.generateKnowledgeBase(self.players, p)
				p.suggestion_number = counter
				p.number = counter
			else:
				p.suggestion_number = counter
				p.number = counter
				print("The cards in your hand are:")
				for card in p.hand:
					print(card.name)
			counter = counter + 1
		if not human:
			gs = self.state.display_state()

	def generateKnowledgeBase(self, players, player):
		knowledgeBase = {}
		for c in self.deck.cards:
			if c in player.hand:
				knowledgeBase[c, 'overall'] = KnowledgeType.G
				knowledgeBase[c, player] = KnowledgeType.B
				temp_players = list(players)
				temp_players.remove(player)
				for p in temp_players:
					knowledgeBase[c, p] = KnowledgeType.C
			else:
				knowledgeBase[c, 'overall'] = KnowledgeType.A
				for p in players:
					knowledgeBase[c, p] = KnowledgeType.A
				knowledgeBase[c, player] = KnowledgeType.C	
		return knowledgeBase
			

	def play(self, display):
		display_output("Game begins")
		rounds = 0
		count = 0 
		while not self.complete:
			result = self.state.move(display)
			if self.state.phase == "solved":
				print(self.state.current_player(), "has solved the game")	
				self.complete = True
				return(self.state.current_player())
			elif self.state.phase == "unsolved":
				print("No players were able to find the cards in the envelope which were: ")
				self.complete = True


DISPLAY_OUTPUT = True 
def display_output(*args):
	if DISPLAY_OUTPUT:
		print(*args)


def start_game(players, randomise, human, deal, display_output=True):

	print("WELCOME TO YOUR GAME OF SPEED CLUE")
	global DISPLAY_OUTPUT
	DISPLAY_OUTPUT = display_output
	if display_output == False:
		print("Running without displaying game output \n")
	if human:	
		print("The players are: ")
		for p in players:
			print(p.name + " : " + p.type)
	if randomise:
		for i in range(secrets.randbelow(10)):
			random.shuffle(players)
	game = Game(players, randomise, human, deal)
	winner = game.play(display_output)
	return winner

def test_series(games, playersList, random, human, deal):
	winner = {}
	for p in playersList:
		winner[p.name] = 0
	os.system('clear')
	for i in range(games):  
		winnerP = start_game(playersList, random, human, deal, display_output=True)
		totalSoFar = winner.get(winnerP.name)
		newWin = totalSoFar + 1
		winner[winnerP.name] = newWin
	for p in playersList:
		print(p.name + " : " + str(winner.get(p.name)))
	totalRounds = 0
	

def game_setup():
	human = False
	games = 0 
	AIPlayers = 0
	randomise = True
	deal = ""
	print("Please setup your game of SPEED CLUE")
	human_player = input("Would you like to include a human player? - please enter Y or N \n")	
	if human_player == "Y":
		human = True
		games = 1
	elif human_player == "N":
		number_of_games = input("How many games would you like to run? - enter a number \n")
		if number_of_games.isnumeric():
			games = number_of_games
		else:
			print("Please enter a number, setup will reset")
			game_setup()
	else:
		print("Please enter Y or N")
		game_setup()


	randomised = input("Would you like randomised deals? - please enter Y or N \n")
	if randomised == "N":
		randomise = False
		if human:
			players = input("How many artificial players would you like to include - please enter the number 2 or 5 \n")
			if players.isnumeric() and (int(players) == 2 or int(players) == 5):
				AIPlayers = int(players)
			else:
				print("Please enter the number 2 or 5, setup will reset")
				game_setup()
		else:
			players = input("How many artificial players would you like to include - please enter the number 3 or 6 \n")
			if players.isnumeric() and (int(players) == 3 or int(players) == 6):
				AIPlayers = int(players)
			else:
				print("Please enter the number 3 or 6, setup will reset")
				game_setup()

	elif randomised == "Y":
		if human:
			players = input("How many artificial players would you like to include - please enter a number between 2-5 \n")
			if players.isnumeric() and int(players) >= 2 and int(players) <= 5:
				AIPlayers = int(players)
			else:
				print("Please enter a number between 2-5, setup will reset")
				game_setup()
		else:
			players = input("How many artificial players would you like to include - please enter a number between 3-6 \n")
			if players.isnumeric() and int(players) >= 3 and int(players) <= 6:
				AIPlayers = int(players)
			else:
				print("Please enter a number between 3-6, setup will reset")
				game_setup()

	else:
		print("Please enter Y or N, setup will reset")
		game_setup()
		
	if not randomise:
		dealSelected = False
		deal = select_deal(AIPlayers, human)
		
	playerNames = ["Alice", "Bob", "Clare", "Dave", "Eve", "Frank"]
	playersList = []
	for player in range(AIPlayers):
		playerStrategy = input("For player " + str(player) + " would you like a basic or advanced artificial player - please enter Basic or Advanced \n")	
		if playerStrategy != "Basic" and playerStrategy != "Advanced":
			print("Please enter Basic or Advanced, setup will reset")
			game_setup()
		else:
			playersList.append(Player(playerNames[player], playerStrategy, "AI"))

	if human:
		humanName = input("Please enter human player's name \n")
		playersList.append(Player(humanName, "Human", "Human"))
	test_series(int(games), playersList, randomise, human, deal)
	exit()

def select_deal(AIPlayers, human):
	deal1 = {"envelope" : ["Professor Plum", "Lead Pipe", "Hall"], 0 : ["Miss Scarlett", "Colonel Mustard", "Rope", "Wrench", "Ballroom", "Library"], 1 : ["Mrs Peacock", "Reverend Green", "Dining Room", "Lounge","Study", "Revolver"], 2 : ["Doctor Orchid", "Dagger", "Candlestick", "Billiard Room", "Conservatory", "Kitchen"]}

	deal2 = {"envelope" : ["Professor Plum", "Wrench", "Kitchen"], 0 : ["Miss Scarlett", "Study", "Library", "Rope", "Lead Pipe", "Reverend Green"], 1 : ["Lounge", "Hall", "Revolver", "Mrs Peacock","Ballroom", "Dining Room"], 2 : ["Colonel Mustard", "Dagger", "Doctor Orchid", "Billiard Room", "Conservatory", "Candlestick"]}

	deal3 = {"envelope" : ["Doctor Orchid", "Rope", "Ballroom"], 0 : ["Reverend Green", "Colonel Mustard", "Revolver", "Lead Pipe", "Hall", "Library"], 1 : ["Mrs Peacock", "Professor Plum", "Dining Room", "Lounge","Study", "Wrench"], 2 : ["Miss Scarlett", "Dagger", "Candlestick", "Billiard Room", "Conservatory", "Kitchen"]}

	deal4 = {"envelope" : ["Miss Scarlett", "Rope", "Kitchen"], 0 : ["Reverend Green", "Colonel Mustard", "Revolver", "Lead Pipe", "Hall", "Library"], 1 : ["Mrs Peacock", "Professor Plum", "Dining Room", "Lounge","Study", "Wrench"], 2 : ["Doctor Orchid", "Dagger", "Candlestick", "Billiard Room", "Conservatory", "Ballroom"]}

	deal5 = {"envelope" : ["Mrs Peacock", "Wrench", "Dining Room"], 0 : ["Miss Scarlett", "Colonel Mustard", "Rope", "Lead Pipe", "Hall", "Library"], 1 : ["Professor Plum", "Reverend Green", "Kitchen", "Lounge","Study", "Revolver"], 2 : ["Doctor Orchid", "Dagger", "Candlestick", "Billiard Room", "Conservatory", "Ballroom"]}

	deal6 = {"envelope" : ["Miss Scarlett", "Revolver", "Hall"], 0 : ["Reverend Green", "Colonel Mustard", "Rope", "Lead Pipe", "Ballroom", "Library"], 1 : ["Mrs Peacock", "Professor Plum", "Dining Room", "Lounge","Study", "Wrench"], 2 : ["Doctor Orchid", "Dagger", "Candlestick", "Billiard Room", "Conservatory", "Kitchen"]}

	dealsList = [deal1, deal2, deal3, deal4, deal5, deal6]
	print("These are your possible deals")
	count = 1
	for deal in dealsList:
		print("DEAL " + str(count))
		print("Envelope : ", end = '' )
		print(*deal.get("envelope"), sep = ", ")
		if AIPlayers == 2 or AIPlayers == 3:	
			#Three players in total
			print("Player 1 (AI) : ", end = '')
			print(*deal.get(0), sep = ", ")
			print("Player 2 (AI) : ", end = '')
			print(*deal.get(1), sep = ", ")
			if human:
				print("Player 3 (human) : ", end = '')
				print(*deal.get(2), sep = ", ")
			else:
				print("Player 3 (AI) : ", end = '')
				print(*deal.get(2), sep = ", ")
			print()
		else:
			#Six players in total
			print("Player 1 (AI) : ", end = '')
			print(*deal.get(0)[0:3], sep = ", ")
			print("Player 2 (AI) : ", end = '')
			print(*deal.get(1)[0:3], sep = ", ")
			print("Player 3 (AI) : ", end = '')
			print(*deal.get(2)[0:3], sep = ", ")
			print("Player 4 (AI) : ", end = '')
			print(*deal.get(0)[3:6], sep = ", ")
			print("Player 5 (AI) : ", end = '')
			print(*deal.get(1)[3:6], sep = ", ")
			if human:
				print("Player 6 (human) : ", end = '')
				print(*deal.get(2)[3:6], sep = ", ")
			else:
				print("Player 6 (AI) : ", end = '')
				print(*deal.get(2)[3:6], sep = ", ")
			print()
		count += 1
	dealSelected = False
	while not dealSelected:
		dealNumber = input("Which deal you would like to use? - please enter a number between 1-6 \n")
		if dealNumber.isnumeric() and int(dealNumber) >= 1 and int(dealNumber) <= 6:
			dealSelected = True
	return dealsList[int(dealNumber) - 1] 
	
game_setup()
