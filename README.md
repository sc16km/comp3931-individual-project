# COMP3931 Individual Project

This is the instructions on how to run and play the game of 'Speed Clue' created for the COMP3931 Individual Project.

To run the program, firstly you will need to have Python 3 installed on your machine and have pulled the code from gitlab. When you run the program if there are any issues of missing libraries (random, sys, secrets, os or enum) you can pip install them, however, they should be included with Python.

SETUP:

To run the program from the terminal, cd into the directory and enter:
python cluedo.py

This should then launch the setup element of the game, you will be prompted to enter:
1) If you would like a human player - Y/N.
2) How many games you would like to run - Enter an integer. (this is only an option if you did not want a human, if a human is involved only 1 game will run)
3) If you would like randomised deals - Y/N.
4) How many artificial players you would like to include - Enter an integer. (If you have selected a human and not randomised deals the integer must be 2 or 5. If you have not selected a human player and not selected random deals the integer must be 3 or 6. If you have selected a human and randomised deals the integer can be 2,3,4 or 5. If you have not selected a human and you have selected random deals the integer can be 3,4,5 or 6).
5) Select the deal - Enter an integer (this only applies if you chose to not have random deals).
6) The strategy for each of the artificial players - Enter either Basic or Advanced.
7) Human player's name - Enter a string (If you have chosen to include a human player).

Each of these inputs will be prompted and they will be validated, if the format of your input is incorrect you will be informed and the setup will restart.

HUMAN PLAYER:

If you choose to include a human player in the game you should provide them with a sheet from the detective's notepad. The human wil be shown the names of the other players and the cards in their hand.

On an opponent's turn, they may be asked to disprove a suggestion, if they hold one of the suggested cards in their hand they should firstly enter Y and then the full name of the card such as Professor Plum, not just Plum. Otherwise they should enter N.

On their turn they should firstly enter if they are ready to accuse - Y/N.
Then enter the suspect, weapon and location cards associated with the accusation (if they entered Y) or the suggestion (if they entered N).
Players will then respond accordingly and the human will again be asked if they are ready to accuse, if they answer N their turn will be over, if they answer Y and they make a wrong accusation they can no longer suggest, otherwise they win the game.
